import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = {
    apiKey: "AIzaSyDsJZQoEm5b0sBuEcQ2ouIHJdWBhoLaRSc",
    authDomain: "opium-store-f0d02.firebaseapp.com",
    databaseURL: "https://opium-store-f0d02-default-rtdb.firebaseio.com",
    projectsku: "opium-store-f0d02",
    storageBucket: "opium-store-f0d02.appspot.com",
    messagingSendersku: "1086697440160",
    appsku: "1:1086697440160:web:2d69d7385f6da31c4e1ac5"
};


const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
/*--------------------------------------------------- */
window.addEventListener('DOMContentLoaded', (event) => {
  Listarproductos();
});
/*--------------------------------------------------- */
var btnAgregar = document.getElementById('btnAgregar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');
/*--------------------------------------------------- */
var vsku = 0;
var vnombre = "";
var vprecio = "";
var vtalla = "";
var vurl = "";
/*--------------------------------------------------- */
function leerInputs() {
  vsku = document.getElementById('txtSku').value;
  vnombre = document.getElementById('txtNombre').value;
  vprecio = document.getElementById('txtPrecio').value;
  vtalla = document.getElementById('txtTalla').value;
  vurl = document.getElementById('url').value;
}
/*--------------------------------------------------- */
function insertarDatos() {
  leerInputs();
  if (vsku === "" || vnombre === "" || vprecio === "" || vtalla === "") {
    alert("Favor de capturar toda la información.");
    return;
  }
/*--------------------------------------------------- */
  set(refS(db, 'productos/' + vsku), {
    nombre: vnombre,
    Precio: vprecio,
    Talla: vtalla,
    url: vurl
  }).then(() => {
    alert("Se insertó con éxito.");
    limpiarInputs();
    Listarproductos();
  }).catch((error) => {
    alert("Ocurrió un error: " + error);
  });
}
/*--------------------------------------------------- */
function buscarDatos() {
  let Sku = document.getElementById('txtSku').value;
  if (Sku === "") {
    Sku = prompt("Ingrese el ID del producto a buscar:");
    if (Sku === null || Sku === "") {
      alert("No se proporcionó un ID válido.");
      return;
    }
  }

  const dbref = refS(db);
  get(child(dbref, 'productos/' + Sku)).then((snapshot) => {
    if (snapshot.exists()) {
      vnombre = snapshot.val().nombre;
      vprecio = snapshot.val().Precio;
      vtalla = snapshot.val().Talla;
      vurl = snapshot.val().url;
      escribirInputs();
    } else {
      alert("El producto con ID " + Sku + " no existe.");
    }
  });
}
/*--------------------------------------------------- */
function escribirInputs() {
  document.getElementById('txtNombre').value = vnombre;
  document.getElementById('txtPrecio').value = vprecio;
  document.getElementById('txtTalla').value = vtalla;
  document.getElementById('url').value = vurl;
}
/*--------------------------------------------------- */
function Listarproductos() {
  const dbRef = refS(db, 'productos');
  const tabla = document.getElementById('tablaProductos');
  const tbody = tabla.querySelector('tbody');
  tbody.innerHTML = '';

  onValue(dbRef, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const data = childSnapshot.val();

      var fila = document.createElement('tr');

      var celdaSku = document.createElement('td');
      celdaSku.textContent = childKey;
      fila.appendChild(celdaSku);

      var celdaNombre = document.createElement('td');
      celdaNombre.textContent = data.nombre;
      fila.appendChild(celdaNombre);

      var celdaTalla = document.createElement('td');
      celdaTalla.textContent = data.Talla;
      fila.appendChild(celdaTalla);

      var celdaPrecio = document.createElement('td');
      celdaPrecio.textContent = "$" + data.Precio;
      fila.appendChild(celdaPrecio);

      var celdaImagen = document.createElement('td');
      var imagen = document.createElement('img');
      imagen.src = data.url;
      imagen.width = 100;
      celdaImagen.appendChild(imagen);
      fila.appendChild(celdaImagen);

      tbody.appendChild(fila);
    });
  }, { onlyOnce: true });
}
/*--------------------------------------------------- */
function actualizarDatos() {
  leerInputs();
  if (vsku === "" || vnombre === "" || vprecio === "" || vtalla === "") {
    alert("Favor de capturar toda la información.");
    return;
  }

  update(refS(db, 'productos/' + vsku), {
    Precio: vprecio,
    nombre: vnombre,
    Talla: vtalla,
    url: vurl
  }).then(() => {
    alert("Se actualizó con éxito.");
    limpiarInputs();
    Listarproductos();
  }).catch((error) => {
    alert("Ocurrió un error: " + error);
  });
}
/*--------------------------------------------------- */
function eliminarProducto() {
  let Sku = document.getElementById('txtSku').value;
  if (Sku === "") {
    Sku = prompt("Ingrese el ID del producto a eliminar:");
    if (Sku === null || Sku === "") {
      alert("No se proporcionó un ID válido.");
      return;
    }
  }

  remove(refS(db, 'productos/' + Sku))
    .then(() => {
      alert("Producto eliminado con éxito.");
      limpiarInputs();
      Listarproductos();
    })
    .catch((error) => {
      alert("Ocurrió un error al eliminar el producto: " + error);
    });
}
/*--------------------------------------------------- */
function limpiarInputs() {
  document.getElementById('txtSku').value = '';
  document.getElementById('txtNombre').value = '';
  document.getElementById('txtPrecio').value = '';
  document.getElementById('txtTalla').value = '';
  document.getElementById('url').value = '';
}
/*--------------------------------------------------- */
function validarNumeros(event) {
  var charCode = event.which ? event.which : event.keyCode;
  if (charCode < 48 || charCode > 57) {
    event.preventDefault();
  }
}
/*--------------------------------------------------- */
function validarPrecio(event) {
  var charCode = event.which ? event.which : event.keyCode;
  if (charCode !== 46 && (charCode < 48 || charCode > 57)) {
    event.preventDefault();
  }
}

document.getElementById('txtSku').addEventListener('keypress', validarNumeros);
document.getElementById('txtPrecio').addEventListener('keypress', validarPrecio);
/*--------------------------------------------------- */
btnBorrar.addEventListener('click', eliminarProducto);
btnAgregar.addEventListener('click', insertarDatos);
btnActualizar.addEventListener('click', actualizarDatos);
btnBuscar.addEventListener('click', buscarDatos);
