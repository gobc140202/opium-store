import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS} from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = {
    apiKey: "AIzaSyDsJZQoEm5b0sBuEcQ2ouIHJdWBhoLaRSc",
    authDomain: "opium-store-f0d02.firebaseapp.com",
    databaseURL: "https://opium-store-f0d02-default-rtdb.firebaseio.com",
    projectsku: "opium-store-f0d02",
    storageBucket: "opium-store-f0d02.appspot.com",
    messagingSendersku: "1086697440160",
    appsku: "1:1086697440160:web:2d69d7385f6da31c4e1ac5"
};
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
/*--------------------------------------------------- */
window.addEventListener('DOMContentLoaded', (event) => {
  mostrarProductos();
});

function mostrarProductos() {
    const dbRef = refS(db, 'productos');
    const contenedorProductos = document.getElementById('contenedorProductos');
  
    contenedorProductos.innerHTML = '';
  
    onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const data = childSnapshot.val();
  
        //crear div
        const divProducto = document.createElement('div');
        divProducto.classList.add('producto');
  
        //anadir img
        const imgProducto = document.createElement('img');
        imgProducto.src = data.url;
        imgProducto.alt = `Producto ${childKey}`;
        imgProducto.classList.add('producto-imagen'); 
        divProducto.appendChild(imgProducto);
  
        // anadir nombre
        const h3Producto = document.createElement('h3');
        h3Producto.textContent = data.nombre;
        divProducto.appendChild(h3Producto);
  
        //anadir precio
        const pPrecio = document.createElement('p');
        pPrecio.id = `precio-producto-${childKey}`;
        pPrecio.textContent = `$${data.Precio}`;
        divProducto.appendChild(pPrecio);
  
        //anadir talla
        const pTalla = document.createElement('p');
        pTalla.textContent = `Talla: ${data.Talla}`;
        divProducto.appendChild(pTalla);
  
        //anadir boton
        const btnAgregarCarrito = document.createElement('button');
        btnAgregarCarrito.classList.add('btn-carrito');
        btnAgregarCarrito.dataset.id = childKey;
        btnAgregarCarrito.textContent = 'Agregar al carrito';
        divProducto.appendChild(btnAgregarCarrito);
  
        //anadir boton
        contenedorProductos.appendChild(divProducto);
      });
    }, { onlyOnce: true });
  }