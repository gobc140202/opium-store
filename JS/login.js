import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js';
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-auth.js';

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDsJZQoEm5b0sBuEcQ2ouIHJdWBhoLaRSc",
    authDomain: "opium-store-f0d02.firebaseapp.com",
    databaseURL: "https://opium-store-f0d02-default-rtdb.firebaseio.com",
    projectsku: "opium-store-f0d02",
    storageBucket: "opium-store-f0d02.appspot.com",
    messagingSendersku: "1086697440160",
    appsku: "1:1086697440160:web:2d69d7385f6da31c4e1ac5"
};


const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);



const formulario = document.getElementById("formulario");
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("password");
const signInButton = document.getElementById("button");
const errorMensaje = document.getElementById("errorMensaje");

formulario.addEventListener("submit", (event) => {
  event.preventDefault(); // Evitar que el formulario se envíe por defecto

  const email = emailInput.value;
  const password = passwordInput.value;

  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {

      window.location.href = "/HTML/opciones.html";
    })
    .catch((error) => {

      console.log("Error al iniciar sesión:", error);

      errorMensaje.textContent = "Credenciales incorrectas. Por favor, intenta nuevamente.";
    });
});

emailInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});

passwordInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});

// ...
